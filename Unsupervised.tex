% !TEX root = Dengue-tweets-classification.tex

\section{Using topic models for clustering tweets}  \label{sec:unsupervised}

As discussed earlier, supervised classification does not fully meet our requirements (Sec. \ref{sec:reqs}), as manual annotation limits the size of training set and makes it difficult to update the model when the characteristics of the epidemics changes. In fact, the characteristics of the last two epidemic waves, which occurred across the two semesters of 2015, have drastically changed. For instance, new symptoms caused by the \textit{Zika} virus have been observed in the epidemic wave, which started in October 2015. The Zika fever is behind a spike in cases of micro-encephalitis, an inflammation of the brain contracted in the first months of pregnancy, which has never before observed in previous epidemic waves in Brazil. The government has already recorded several adult deaths and more than 2,000 cases of the disease, which can stunt the growth of the foetus's head. Such types of epidemic changes drastically change the nature of Twitter content. Also, finding a crisp, unambiguous classification has been problematic.

Therefore, in this section we describe a complementary, unsupervised classification approach, based on Latent Dirichlet Allocation (LDA) \cite{Blei:2003:LDA:944919.944937}.
%
This is a popular form of \textit{semantic clustering}, so called because each cluster is described by a ranked lists of terms, called ``topics'', making it easy for experts to assign an intuitive meaning to them.
%
As mentioned, LDA has been used before for Twitter content analysis and topic discovery, for example by \cite{Morstatter2013,Rosa2011,Weng2010}.
What we investigate is an application of LDA that shows the potential for scalability and flexibility, i.e., by periodically rebuilding the clusters to track drift in Twitter search keywords.

Our sample dataset consists of $107,376$ tweets, harvested in summer 2015, containing a total of $17,191$ unique words.
%
Raw tweets were pre-processed just like for classification (phase 2 in Fig. \ref{fig:Denguestrategy}), 
producing a bag-of-words representation of each tweet. 
Additionally, as a further curation step we removed the $20$ most frequent words in the dataset, as well as all words that do not recur in at least two tweets.
This last step is needed to prevent very frequent terms from appearing in all topics, which reduces the effect of our cluster quality metrices and cluster intelligibility.

\subsection{Evaluation of clustering quality}

We explored a space of clustering schemes ranging from 2 to 8 clusters.\footnote{All experiments carried out using the Apache Spark LDA package \small \url{https://spark.apache.org/docs/latest/mllib-clustering.html#latent-dirichlet-allocation-lda}}
%
In the absence of an accepted gold standard, a number of evaluation methods have been proposed in the literature.
For instance, \cite{Morstatter2013} proposes to measure cluster quality by quantifying the differences caused in topic mining by two different stream sampling mechanisms.
The method is based on the differences between the distribution of words across topics and between the two sampling mechanisms. 
However, it cannot be used in our setting, because our corpus of tweets is fixed, rather than a sample.
%
Also, while any two individual words may have different frequency distributions, the approach does not necessarily take into account the importance, measured by relative frequency, of the word within the entire corpus.
%
In an alternative approach, \cite{Rosa2011} use ground truth in the form of pre-established hashtags.
%
This is not applicable in our scenario, either, because by the way our topic filtering is done, most of the tweets in our corpus will already include a high number of hashtags, including for instance the \#dengue hashtag.

Instead, we propose to use \textit{intra-} and \textit{inter-} cluster similarity as our main evaluation criteria.
This is inspired by \textit{silhouettes} \cite{Rousseeuw1986}, and based on the contrast between \textit{tightness} (how similar data are to each other in a cluster) and \textit{separation} (how dissimilar data are across clusters).
%
Specifically, we define the similarity between two clusters $C_a, C_b$ in terms of the cosine TF-IDF similarity of each pair of tweets they contain, i.e., $t_i \in C_a$ and $t_j \in C_b$, as follows:
\begin{equation}
	\mathit{sim}(C_a, C_b) = \frac{1}{|C_a| \; |C_b|} \sum_{t_i \in C_a, t_j \in C_b}\frac{\mathbf{v}(t_i) \cdot \mathbf{v}(t_j)}{||\mathbf{v}(t_i)|| \; ||\mathbf{v}(t_j)||}
\label{eq:similarity}
\end{equation}
where $\mathbf{v}(t_i)$ is the TF-IDF vector representation of a tweet. That is, %after ordering all unique terms in the corpus,
the $k$th element of the vector, $t_i[k]$, is the TF-IDF score of the $k$th term. As a reminder, the TF-IDF score of a term quantifies the relative importance of a term within a corpus of documents \cite{Aggarwal2012}. Eq. (\ref{eq:similarity}) defines the \textit{inter-cluster similarity} between two clusters $C_a \neq C_b$, while the \textit{intra-cluster similarity} of a cluster $C$ is obtained by setting $C_a = C_b = C$.

Fig.\ \ref{fig:intra.inter} reports the inter- and intra-cluster similarity scores for each choice of clustering scheme.
The absolute similarity numbers are small, probably due to the sparse nature of tweets and the overall little linguistic overlap within clusters.
However, we can see that the intra-cluster similarity is more than twice the inter-cluster similarity, indicating good separation amongst the clusters across all configurations.
This seems to confirm that the LDA approach serves the purpose of discovering sub-topics of interest within an already focused general topic, defined by a set of keywords.
%

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{graphics/unsupervised/intra-inter.pdf}
	\caption{Intra- and Inter-cluster similarities}
	\label{fig:intra.inter}
\end{figure}


%\begin{figure}
%	\centering
%	\includegraphics[width=\linewidth]{graphics/unsupervised/eval-4-clusters.pdf}
%	\caption{TF-IDF evaluation for 4 clusters}
%	\label{fig:eval.4.clusters}
%\end{figure}

\begin{figure*}[ht]
\centering
\begin{minipage}[b]{0.4\linewidth}
	\includegraphics[width=\linewidth]{graphics/unsupervised/eval-2-clusters-ticks.pdf}
\end{minipage}
\quad
\begin{minipage}[b]{0.4\linewidth}
	\includegraphics[width=\linewidth]{graphics/unsupervised/eval-4-clusters-ticks.pdf}
\end{minipage}
\quad
\begin{minipage}[b]{0.4\linewidth}
	\includegraphics[width=\linewidth]{graphics/unsupervised/eval-6-clusters-ticks.pdf}
\end{minipage}
	\caption{Inter- and intra-similarity for 2, 4, and 6 clusters topic models}
	\label{fig:clusters}
	\end{figure*}

%\begin{figure}
%	\centering
%	\includegraphics[width=\linewidth]{graphics/unsupervised/eval-2-clusters-ticks.pdf}
%	\caption{Evaluation for 2 clusters}
%	\label{fig:eval.2.clusters}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\includegraphics[width=\linewidth]{graphics/unsupervised/eval-4-clusters-ticks.pdf}
%	\caption{Evaluation for 4 clusters}
%	\label{fig:eval.4.clusters}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\includegraphics[width=\linewidth]{graphics/unsupervised/eval-6-clusters-ticks.pdf}
%	\caption{evaluation for 6 clusters}
%	\label{fig:eval.6.clusters}
%\end{figure}

The plots in Fig.\ \ref{fig:clusters} provide more detailed indication of the contrast between intra- and inter-cluster similarity at the level of detail of individual clusters.
%
For example, in the 4-clusters case, the average of the diagonal values of the raster plot is intra-cluster similarity reported in Fig.\ \ref{fig:intra.inter}, whereas the average of the off-diagonal values represent the inter-cluster similarity. In these plots, darker boxes indicates higher (average) similarity. 
Thus, a plot where the diagonals are darker than the off-diagonal elements is an indication of a high quality clustering scheme.

\begin{table}
\begin{tiny}
\centering
\begin{tabular}{|c|c|c|c|}

	\hline 
	\textbf{Topic 1} & \textbf{Topic 2} & \textbf{Topic 3} & \textbf{Topic 4} \\	
	\hline	
	% 1-5
	n\~{a}o  & \`{a}        & contra     & mosquito \\
	ir       & ano          & dizer      & eu       \\
	ta       & combate      & novo       & se       \\
	meu      & morte        & mas        & epidemia \\
	parado   & mil          & j\'{a}     & dia      \\

	% 6-10
	\'{a}gua & s\'{a}ude    & cidade     & pegar    \\
	poder    & sp           & suspeito   & foco     \\
	esse     & confirma     & todo       & me       \\
	fazer    & n\'{u}mero   & sa\'{u}de  & nem      \\
	t\~{a}o  & secretaria   & doen\c{c}a & ele      \\

	% 11-15
	voc\^{e} & a\c{c}\~{a}o & zika       & s\'{o}   \\
	dar      & homem        & bairro     & hoje     \\
	t\'{a}   & chegar       & morrer     & casa     \\
	vacina   & registro     & porque     & igual    \\
	wpp      & rio          & combater   & acho     \\
	
	% 16-20
	parada   & agente       & grande     & quer     \\
	at\'{e}  & d            & for\c{c}a  & m        \\
	como     & paulo        & mutiro     & te       \\
	ficar    & confirmado   & brasil     & mesmo    \\
	ma       & queda        & mau        & estado   \\
	\hline
\end{tabular}
\caption{Top-20 words for each topic.}
\label{table:topics}
\end{tiny}
\end{table}

Although the similarity metrics are objective and seems to confirm the good quality of the clustering, the plots in Fig. \ref{fig:intra.inter} and Fig. \ref{fig:clusters} do not provide much insight into the optimal number of clusters, or indeed their semantic interpretation.
We therefore relied on our domain experts for the empirical selection of the clustering scheme (2,4,6,8 clusters) that would most closely lend itself to an intuitive semantic interpretation of the topics.
%
Their assessment is reported below.
Furthermore, we also carried out an objective comparison of topics content using our four classes model as a frame of reference. 
This is described in Sec. \ref{sec:comparison}. 
Based on these two assessments, in Sec. \ref{sec:discussion} we discuss the merits of the clustering approach relative to those of the classifier.

\subsection{Empirical topics interpretation}

Expert inspection, carried out by native Brazilian Portuguese speakers, considered both the list of words within each topic, and a sample of the tweets for that topic.
In this case, the most intelligible clustering scheme had $4$ topics.
%
%The TF-IDF similarity between the four topics is illustrated in Fig.\ \ref{fig:eval.4.clusters} which reflects the intuition behind the previous result in Fig.\ \ref{fig:intra.inter}: namely that tweets in any cluster bear more similarity to the tweets in the same cluster than to the tweets in other clusters.
%
Table\ \ref{table:topics} presents, for each topic, the top $20$ words, as identified by LDA for this scheme.
%
The importance of the words is given by LDA as a measure of how well they are represented in the topics.\footnote{Some of the words are just noise. This is due to occasional imperfect lemmatisation during the preprocessing stage.}


Unsurprisingly, topic inspection suggests an interpretation that only partially overlaps with the a priori classification we have seen in the supervised case.
Specifically, \textbf{Topic 1} is closely related to \textit{Jokes}.
Most of the tweets for this topic either make an analogy between Dengue and the users lives, or they use the words related to Dengue as a pun.
A typical pattern is the following:
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize meu [algo como: wpp - WhatsApp, timeline, Facebook, twitter etc] est\'{a} mais parado do que agua com dengue.\\
\textit{My [something like: wpp - WhatsApp, timeline, Facebook, twitter etc] is more still than standing water with dengue mosquito.}
\end{mdframed}
%
Specific examples are:
%
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Aitizapi ta com dengue de t\~{a}o parado  \\
\textit{Aitizapi is so still that it has been infected by dengue}
\end{mdframed}
%
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Concession\'{a}ria t\'{a} dando dengue de t\~{a}o parada que t\'{a} \\
\textit{Car dealership is so still that it has dengue}
\end{mdframed}
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Decretado, meu wpp deu dengue \\
\textit{It is official now, my wpp has dengue}
\end{mdframed}

In the first example above, the user was playing with the words when referring to the standing status and inactivity in his Whatsapp account. Breeding sites of the Aedes mosquito are mostly found in containers with standing water. The third example uses similar sacarsm.
In the second example, the user is joking about significant decreases in car purchases due to the emerging economic crisis in Brazil. For the aforementioned reasons, many of the jokes in the last epidemic wave have been related to Zika, which in Brazilian Portuguese, has been used as a new slang word for failure or any kind of personal problem.    

\textbf{Topic 2} is interpreted as \textit{news} about increase or decrease of Aedes-borne disease cases as well as specific cases of people who died because of the Aedes-borne diseases, i.e. Dengue, Chikungunya and Zika. It also contains news about the combat of the mosquito in certain locations as well. Examples:
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Rio Preto registra mais de 11 mil casos de dengue e 10 mortes no ano  \#SP\\
\textit{Rio Preto reports more than 11 thousand cases of dengue in the year \#SP}
\end{mdframed}
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize 543 casos est\~{a}o em an\'{a}lise - Londrina confirma mais de 2,5 mil casos de dengue em 2015 - [URL removed]\\
\textit{543 cases of dengue are under analysis - Londrina confirms more than 2.5 cases of dengue in 2015 - [URL removed]}
\end{mdframed}
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Limeira confirma mais uma morte por dengue e chega a 17 [URL removed]\\
\textit{Limeira confirms another death because of dengue and the total reaches 17 [URL removed]}
\end{mdframed}

\textbf{Topic 3} appears to contain mostly \textit{news about campaigns} or actions to combat or to prevent Aedes-borne diseases, for instance:
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Curcuma contra dengue [URL removed]\\
\textit{Curcuma against dengue}
\end{mdframed}
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Prefeitura de Carapicuíba realiza nova campanha contra dengue e CHIKUNGUNYA[URL removed] \\
\textit{Carapicuíba City Hall launches new campaign against dengue and CHIKUNGUNYA[URL removed]}
\end{mdframed}
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Bairros de Barbacena recebem mutirões contra a dengue - [URL removed] \\
\textit{Barcelona Neighborhoods receive collective effort to cambat dengue [URL removed]}
\end{mdframed}


The difference between the news in topics 2 and 3 concerns the type of news, which for topic 2 is mostly about the increase or decrease of Aedes-borne diseases, whereas in topic 3 is about campaigns or actions to combat the propagation of the Aedes mosquito.

Finally, \textbf{Topic 4} contains mostly \textit{sickness} tweets, with some instances of \textit{jokes}:
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\small Eu To com dengue \\
\textit{I have dengue fever}
\end{mdframed}


\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\small Ser\'{a} que eu to com dengue ?\\
\textit{I wonder: do I have dengue?}
\end{mdframed}

\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\small Hoje eu t\^{o} igual a mosquito da Dengue... cheio de casos por a\'{\i}! \\
\textit{Today I’m feeling like the dengue mosquito… full of cases out there!}
\end{mdframed}
Note that, in this particular tweet, the user makes a pun with the word ``case''. 
In Brazilian Portuguese, this word has dual interpretation, either as an actual case of Dengue fever, or a love-affair as well. The intended meaning of the user is probably the latter.


\subsection{Classes vs clusters}  \label{sec:comparison}

The point to note in the assessment above is that the most relevant tweets, those corresponding to the \textit{Mosquito Focus} class, are not easily spotted, in particular they do no seem to characterise  any of the topics.
Intuitively, this can be explained in terms of the relative scarcity of these tweets within the stream, combined with the balancing across topics that occurs within LDA. 

In order to quantify this intuition, we have analysed the topics content using our pre-defined four classes as a frame of reference.
In this analysis, we have used our trained classifier to predict the class labels of all the tweets in the corpus that we used to generate the topics (about 100,000). 
We then counted the proportion of class labels in each topic, as well as, for each class, the scattering of the class labels across the topics.
The results are presented in Table \ref{tab:classesByCluster} and Table \ref{tab:classesAcrossClusters}, respectively, where the dominant entries for each column (resp row) are emphasised.

\begin{table*}[ht]
\centering
\begin{minipage}[b]{0.45\linewidth}
\begin{scriptsize}
\begin{tabular}{|c|c|c|c|c|}
\hline  &  Topic 1 &  Topic 2 & Topic 3  & Topic 4  \\ 
\hline  News &  13.9 &  \textbf{72.6} &  27.2 &  \textbf{39.4} \\ 
\hline  Joke &  \textbf{39.5} &  0.1 &  2.8 &  4.1 \\ 
\hline  Mosquito Focus &  30 &  4.0 &  12.3 &  12.5 \\ 
\hline  Sickness &  16.6  &  23.3 &  \textbf{57.7} &  \textbf{44.0} \\ 
\hhline{|=|=|=|=|=|}  Total  &  100 &  100 &  100 & 100  \\ 
\hline 
\end{tabular} 
\end{scriptsize}
\caption{Distribution (\%) of predicted class labels within each cluster}
\label{tab:classesByCluster}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
\begin{scriptsize}
\begin{tabular}{|c|c|c|c|c||c|}
\hline  &  Topic 1 &  Topic 2 & Topic 3  & Topic 4 & Total  \\ 
\hline  News &  29.1 &  28.5 &  8.9 &  33.5  & 100 \\ 
\hline  Joke &  \textbf{95.0} &  0.03 &  1.05 &  4.0   & 100 \\ 
\hline  Mosquito Focus &  \textbf{79.5} &  2.0 &  5.1 &  13.4   & 100 \\ 
\hline  Sickness &  \textbf{34.8}  &  9.1 &  18.8 &  \textbf{37.3}   & 100 \\ 
\hline 
\end{tabular}
\end{scriptsize}
\caption{Scattering (\%) of predicted class labels across clusters}
\label{tab:classesAcrossClusters} 
\end{minipage}
\end{table*}
	



It is worth remembering that these results are based on predicted class labels and are therefore inherently subject to the classifier's inaccuracy.
Furthermore, the predicted class labels were \textit{not} available to experts when they inspected topic content, thus they effectively performed a new manual classification on a content sample for each topic.
Despite the inaccuracies introduced by these elements, Table \ref{tab:classesByCluster} seems to corroborate the experts' assessment regarding topics 1 and 2, but less so for topics 3 and 4.
This may be due to the sampling operated by the experts, which selected content towards the top of the topic (LDA ranks content by relevance within a topic) and may have come across joke entries which are otherwise scarce in topic 4.

Although the heavy concentration on joke tweets in topic 1 from Table \ref{tab:classesByCluster} seems promising (i.e., the other topics are relatively noise-free), Table \ref{tab:classesAcrossClusters} shows a problem, namely that topic 1 is also where the vast majority of \textit{Mosquito Focus} tweets are found. 
Thus, although topic 1 segregates the most informative tweets well, it is also very noisy, as these tweets are relatively scarce within the entire corpus.

\subsection{Discussion}\label{sec:discussion}

The analysis just described suggests that topic modelling offers less control over the content of topics, compared to a traditional classifier, especially on a naturally noisy media channel. 
Although relevant content can be ascribed to specific topics, these are polluted by noise.
Despite this, LDA performs relatively well on creating sub-topics from a sample that is already focused on a specific topic, such as conversations on the Aedes-transmitted viruses.

The main appeal of the classifier is that it makes it straightforward to select relevant content, with acceptable experimental accuracy.
In our follow on research we are investigating ways to combine the benefits of the two approaches.
%
Specifically, we are studying a unified semi-supervised model where topic modelling can be used to improve the accuracy of the classifier, i.e., by automatically expanding the training set, and to alleviate the cost of re-training at the same time.

