\section{Introduction}

%Leveraging Implicit and Explicit Crowdsourcing for Rapid Combat of Epidemic Waves
               

\subsection{Motivation}                 

Mosquito-borne disease epidemics are increasingly becoming more frequent and diverse around
the globe and it is likely that this is only the early stage of epidemic waves that will continue for several 
decades. Rapidly spreading diseases to combat nowadays are those transmitted by the \textit{Aedes} mosquitoes \cite{Denguecenter2015}. Aedes mosquitoes carry not only \textit{Dengue} virus, but also \textit{Chikungunya} and \textit{Zika} viruses \cite{Denguecenter2015}, which are responsible for thousands of deaths every year. Therefore, improved surveillance through rapid response measures against Aedes-borne diseases is a long-standing tenet 
to various health systems around the world. They are urgently required to mitigating the already 
heavy burden on those health systems and limiting further spread of mosquito-borne diseases within geographical locations, such as in Brazil. Control of Aedes-borne disease requires the vector control 
by identifying and reducing breeding sites, which essentially requires efficient community participation. 
It is widely recognized that response measures cannot scale up without active community participation
on identifying and communicating breeding sites. Key to this process is the identification of citizens that 
can contribute with this information in the outset of each epidemic wave.


With these premises, the long-term goal of our work is twofold. 
Firstly, we want to be able to rapidly and automatically identify mosquito breeding sites, so that appropriate and timely response measures can be taken.
Secondly, we want to be able to automatically and reliably identify individuals or organisations who are likely to be receptive to specific
requests for active engagement in health vigilance, and ultimately to work with the health authorities. 
Paraphrasing \cite{Sakaki2010}, we refer to these as \textit{informative social sensors}.

\subsection{Role of Twitter content classification}

Our hypothesis is that existing techniques for automatic detection of relevant content in Twitter can be exploited to achieve both of our goals: (i) select actionable information from the broad stream feed, and (ii) identify the most informative social sensors within a local community.
%
Previous work, e.g. \cite{Gomide2011,Lampos2010,Achrekar2010}, has identified the potential of social media channels, such as Twitter, on offering continuous source of epidemic information, arming public health systems with the ability to perform real-time surveillance. 

\begin{figure*}[htb]
\centering
\includegraphics[width=.75\linewidth]{./graphics/Dengue-strategy}
\caption{Role of automated Twitter relevance detection for interventions and engagement in health vigilance against Dengue }
\label{fig:Denguestrategy}
\end{figure*}

However, previously proposed approaches are often limited or insufficient for rapid combat of epidemic waves for several reasons. Firstly, previous work has mainly explored the use 
of social media channels to predict Dengue cases and outbreak patterns by exploring disease-related 
posts from previous outbreaks. However, the combination of socio-economic, environmental and ecological 
factors dramatically changes the characteristics governing each epidemic wave. As a consequence, exploring disease-related posts from previous outbreaks tends to be ineffective to identify breeding
sites in the outset of each outbreak. Secondly, previous work is not aimed at identifying map breeding sites of the mosquito within a region, and to identify the best information providers within a 
community.


The role of Twitter content relevance detection is depicted in Fig. \ref{fig:Denguestrategy}, in the context of our broader strategy.
%
Social sensors, the people in the upper half of the figure, contribute information either implicitly, i.e., by spontaneously carrying out public conversations on social media channels, or explicitly, i.e., by interacting with dedicate public Web portals and mobile apps. 
As an example, our group %in Brazil 
has been developing both such a Dengue mapping portal, and a mobile app that members of the public may use to report cases of Dengue in their local areas \textbf{[BLIND FOR REVIEW]}.% \cite{VazaDengue2015}.

As shown in the lower part of the figure, we monitor the Twitter feed, pre-select tweets according to a broad description of the Dengue topics using keywords, then classify the selected tweets, aiming to segregate relevant signal from the noise. 
We distinguish between relevant signal that is \textit{directly} and \textit{indirectly} actionable. Directly actionable tweets, which we classify as \textit{mosquito focus}, are those that contain sufficient information regarding a breeding site (including geo-location), to inform immediate  interventions by the health authorities.
For instance:
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize @Ligue1746 Aten\c{c}\~{a}o! Foco no mosquito da dengue. Av Sta Cruz, Bangu. Em frente ao hospital S\~{a}o Louren\c{c}o! 
(\textit{@Ligue1746 Attention! Mosquito focus found in Santa Cruz avenue, Bangu. In front of the São Lourenço hospital!})
\end{mdframed}
These posts are relatively scarce within the overall stream, however, accounting for about 16\% of the ground truth class assignments.

Indirectly actionable tweets carry more generic information about members of the public complaining about being affected by Dengue (the \textit{Sickness} class), or \textit{News} about the current Dengue epidemics.
For example:
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Eu To com dengue 
(\textit{I have dengue fever})
\end{mdframed}
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize ES  tem mais de 21 mil casos de dengue em 2015
(\textit{ES has more than 21 thousands cases of dengue in 2015})
\end{mdframed}

The rest of the tweets are all considered noise. In particular, these include messages where people joke about Dengue in a sarcastic tone, which is commonly used in online conversation in Brazil, for example:
\begin{mdframed}[leftmargin=\parindent,rightmargin=\parindent,skipabove=\topsep,skipbelow=\topsep]  
\scriptsize Meu WhatsApp ta t\~{a}o parado que vai criar mosquito da dengue 
(\textit{My WhatsAp is so still that it’ll create dengue mosquito})
\end{mdframed}

Our current work, which is the focus of this paper, has been on developing a filtering component, identified as (1, 2, and 3.1, 3.2) in Fig. \ref{fig:Denguestrategy}, for automatically classifying directly and indirectly actionable tweets. This is used to feed our Web portal, making sure the amount of noise on the public pages is minimised.
Our broader, and longer term goal, is to use indirectly actionable tweets to identify and rank the most informative social sensors. The ranking will rely on the content of the Tweets (for instance, how regularly certain users post relevant messages) in combination with the topology of the Twitter social graph the users belong to, for instance, their connections to other active users or organisations. 
This predictive functionality, which is still in the research phase and will not be discussed further in this paper, is indicated as (4) in  Fig. \ref{fig:Denguestrategy}.
Ultimately, we expect that reliable prediction of good social sensors will translate into more efficient use of limited community engagement resources.

\subsection{Requirements and challenges}  \label{sec:reqs}

The existing type of Twitter content about Dengue makes it challenging to realise an effective content filtering component for our architecture. Firstly, \textit{epidemic waves} differ from season to season, requiring different keyword settings and filtering from the Twitter feed, in order to accurately track an epidemic. Examples of keywords for tracking different virus and new emerging symptoms include \textit{Dengue}, \textit{Chikunguya}, and \textit{Zika}. Simply taking the union of all three would just add to the noise. What is required instead is the ability to rapidly reconfigure the classifier following a drift in topic.

Secondly, content pre-processing (phase 2) must be specifically adapted not just to the Twitter jargon, but also to the local cultural nuances of Brazilian online social discourse.  
This includes the abundant use of sarcastic tone, which must be reliably detected as, in our context, it is just a form of noise.
Colloquial style may also drift as social interactions about a topic evolve over time. 

Finally, finding a clear separation amongst classes, i.e., a robust operational definition of \textit{relevance} of Twitter content in our context, has proven difficult for the domain and language experts in Brazil who have prepared the training set for the supervised classifier.
In practice, the boundaries between the four classes are often blurred, i.e., some of the messages may reasonably be assigned to more than one class.

\subsection{Approach and Contributions}

In view of these requirements, our approach has been to experiment with two complementary ways to detect content relevance. 
After harvesting messages from the Twitter stream using an adaptive set of keywords for basic filtering (step 1 in Fig. \ref{fig:Denguestrategy}), and following a content pre-processing stage (2), we then classify the tweets in two separate ways.
The first is a traditional supervised classifier, trained using a manually annotated set of examples (3.1 in Fig. \ref{fig:Denguestrategy}).
The second is an unsupervised approach, based on Topic Models (3.2) and specifically on LDA \cite{Blei:2003:LDA:944919.944937}, an algorithm that has been previously shown to apply well to clustering Twitter data \cite{ramage2010characterizing,Ritter2012}.

Intuitively, we expect supervised classification to provide good accuracy, as well as give an obvious way to select actionable content from the most informative classes (\textit{mosquito focus}, \textit{sickness}, and \textit{News} in this order). 
On the other hand, this model suffers from known limitations in the size of the training set, which may lead to disappointing performance on content in the wild, and it is expensive to re-train following changes in the filtering keywords.

In contrast, topic modelling is a form of semantic clustering where a clustering scheme can be easily periodically re-generated from large samples. 
We may use the scheme to classify previously unseen instances, using the similarity metrics defined in Sec. \ref{sec:unsupervised}.
The main advantage of topic models over other clustering models is that clusters are described using ranked lists of terms from the content's vocabulary (topics). On the other hand, a topic may include heterogeneous content that cuts across expert-defined classes, such as those above, making it harder to associate them with a clear focus. 
This problem is particularly acute in our setting, where we already have a topic defined (through keywords), and we are essentially asking LDA to further refine it in terms of well-separated sub-topics.

Our specific contributions in this paper are: (i) a pipeline that implements both methods, including a dedicated pre-processing phase that accounts for idiosincratic use of the Brazilian Portuguese language in tweets, (ii) an experimental evaluation of their effectiveness, and (iii) a critical comparison of the two approaches vis a vis the requirements above. 
The supervised classifier is currently in operation as part of the experimental Dengue Web portal developed at \textbf{[BLIND FOR REVIEW]}. % PUC-Rio \cite{VazaDengue2015}.

\subsection{Implementation and overview of the results}

We assessed the potential of our approach on large cities of Brazil, such as Rio de Janeiro, by analyzing two cycles of Aedes-related epidemic waves. For our supervised classifier, presented in detail in Sec.\ref{sec:supervised}, we settled for a Naive Bayes model after experimenting with SVM and MaxEntropy models, which gave slightly worse performance on standard k-fold cross validation. 
Despite using a small training set of about 1,000 messages, limited in size by the availability of manual annotators, we achieved an average 84.4\% accuracy.
To further validate the accuracy we then classified, both manually and automatically, a new set of previously unseen 1,600 messages.
On this set, disjoint from that used for training, our results indicate around 75\% agreement between the
automatic and the manual classification. Detailed results are presented in Table \ref{tab:supervised-results}, Sec. \ref{sec:supervised}.

LDA-based clustering \cite{Blei:2003:LDA:944919.944937} used a sample consisting of about 100,000 Twitter messages, previously selected using standard keyword filtering from the Twitter feed during the same season as the earlier training set (from the first semester of 2015), and pre-processed in the same way as the classifier training set.

As reported in detail in Sec. \ref{sec:unsupervised}, cluster cohesion and separation metrics, based on \textit{silhouettes} \cite{Rousseeuw1986} and TF-IDF similarity, show that any two tweets within a cluster are on average at least twice as mutually similar (intra-cluster similarity) than tweets from two different clusters (inter-cluster similarity).
Expert assessment, clearly much less expensive than extensive manual annotation, was used to select both the most semantically significant topic model (4 clusters) and the interpretation of the topics in terms of actionable/noise separation of the content.

The main conclusion we draw from these experiments is that it would be difficult to use whole-clusters as a filtering criteria for selecting actionable content, because each of the clusters contains a fair amount of noise.
Relatively scarce high-value content, such as that found in the \textit{Mosquito Focus} tweets is particularly vulnerable to this problem because, although most of these tweets are found in one topic, that topic also contains many unrelated tweets.
This is most likely due to the balancing across topics that occurs as part of LDA processing.
We discuss these results in Sec. \ref{sec:discussion}.

\input{RelatedWork}
 

